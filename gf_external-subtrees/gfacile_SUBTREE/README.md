# Gfacile


related to gf - Gfacile  : https://framagit.org/boblib/gf_git_facile 
and aimed to be inserted in it as a subtree



##This is a subproject of gf Gfacile : 

Main goal : propose detailled tutorials of Gfacile


Gfacile
    Copyright (C) 2019  Nicolas Ival

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
