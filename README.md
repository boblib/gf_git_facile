echo 'This file is the README.md  for gf (G facile)  and it can not be executed as an instruction '
exit

------------------------------------------------------

# gf G Facil
# gf G Facile
#    Git Facilitator

##    VERSION   ALPHA        still in active changes !   but already functionning most of the time :)

      Note ; stable versions are to be found in branch    stable_only   
               https://framagit.org/boblib/gf_git_facile/commits/stable_only

   gf (G-facile) is a simple ergonomic rewording of GIT instructions, and providing some status and synthesis info tables,
   for linux terminal use, in bash, very simple...

**learn and use GIT in a simple way....**



## Licence :

    gf ( G facile ) files are under :   

   copyright 2019 (c) Nicolas Ival

and

   licence  GNU GPL 3.0 or later    (copy provided at the bottom of this README.md  file)

    This program (and all functions files part of it) is free software:
    you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



## NOTE : gf is using GIT instructions,
        and GIT is under licence GNU GPL 2.0 or later ,        
        ...therefore licence compatibility should be ok (as far as I understand)



## DISCLAIMER
	this software is used without any garanty, and all the more that it is in developpement stages...



## gf  objectives :

facilitate use of GIT for beginners,
showing how GIT stores data, and the workflow from working status to index (staged) to head (commited) and to upstream (distant repo)

give easy acces to simple functions, and ways to revert actions

show the GIT syntax equivalent to the gf functions, so that standard GIT syntax is learned while using gf
and show the GIT standard output of the original function (printed in white, whereas other synthesis are in colors)

provide a clear synthesis view of the repo updated after every action (as per gf_visual_files-synthesis-table) : *if no file is shown, it means every file is already pushed and in equivalent state localy and remotely... therefore nothing extra to do.*


![Picture of example screen](https://framagit.org/boblib/gf_git_facile/-/blob/3097368302c855ffbf6c43d65762467c951079b0/table%20of%20files%20and%20their%20git%20status.png?raw=true "screen_example")


and
eventuelly inspire GIT community to include these intuitive worflow arrows, and the synthesis views as per visual-synthesis functions (for files, branches, submodules, and upstreams), and why not devellop it into a graphic interface similar to GIT GUI for instance.


## Note regarding the code :

gf is availlable and tested on LINUX (not so many distribution for the moment...)

it seems it might also be used on mac and windows but not looked at ...
	https://gitforwindows.org/



remark : I'm not a professional programmer, and therefore some mistakes or bad usages may (are still) present in the code.
         please be forgiving, and feel free to propose ameliorations.


## gf installation

  copy (or clone) all files into any directory, for instance   mydirectory
      the directory containing the instructions = mydirectory/gf_instructions      must be made into the PATH

      to do that see for instance :       http://www.troubleshooters.com/linux/prepostpath.htm

**easy method to add gf_ localy (to the current terminal)**
 
go to gf_  directory (where you cloned it)
  cd directory/containing/readme
and type in terminal!
  export PATH=$PATH:/usr/sbin




and other details...


      basicaly, to add the instructions permanently to all users exept root :    
		echo $PATH            #  shows all directories actually in the PATH

		edit the file     etc/profile and add at the end :

			PATH=$PATH:/directory_chain.../mydirectory/gf_instructions            #  ..this is the directory where the instructions are.
			export PATH

		close session and open a new session
		
      or (other solution working on some linux : (which are using etc/profile.d	)
      		sudo gedit /etc/profile.d/gf_git_facile_instructions.sh             
      		  ou autre instructions pour créer le fichier gf_git_facile_instructions.sh   
      		                                                dans le répertoire /etc/profile.d/
                et ajouter dans ce fichier :  
                PATH=$PATH:/path-to-directory-gf_git_facile/gf_instructions
                         where /path-to-directory-gf_git_facile/ is the path to directory holding readme file and
                          directory  gf_instructions
      	
      		sudo cat >> /etc/profile.d/gf_git_facile_instructions.sh << \EOF
      		PATH=$PATH:/path-to-directory-gf_git_facile/gf_instructions
      		EOF
		
	or other method :
	     Global paths should be set in /etc/profile or /etc/environment, 
	     just add this line to /etc/profile:
	     PATH=$PATH:/path/to/ANT/bin
	     (vu sur https://unix.stackexchange.com/questions/104727/add-a-path-in-path-globally-for-every-user )

	
Notes : 	
To add temporarily the gf_  instructions (in the current terminal) use :
We can add a new path to the PATH variable using the export command.
To prepend a new path, such as /some/new/path, we reassign the PATH variable with 
our new path at the beginning of the existing PATH variable (represented by $PATH):
export PATH=/some/new/path:$PATH
To append a new path, we reassign PATH with the new path at the end:
export PATH=$PATH:/some/new/path
but when we use the export command and open a new shell, the added path is lost.
	
4.1. Locally

To persist our changes for the current user, we add our export command to the end of ~/.profile. If the ~/.profile file doesn’t exist, we should create it using the touch command:
touch ~/.profile

Then we can add our export command to ~/.profile.
Additionally, we need to open a new shell, or source our ~/.profile file to reflect the change. We’d execute:
. ~/.profile

    Or if we’re using Bash, we could use the source command:
       source ~/.profile  

We could also append our export command to ~/.bash_profile if we’re using Bash, but our changes won’t be reflected in other shells, such as Zsh. 
We shouldn’t add our export command to ~/.bashrc because only interactive Bash shells read this configuration file. If we open a non-interactive shell or a shell other than Bash, our PATH change won’t be reflected.	
		


**BEST install METHOD :**   seen on https://www.baeldung.com/linux/path-variable
**Globally**
We can add a new path for all users on a Unix-like system by creating a file ending in .sh in /etc/profile.d/ and adding our export command to this file.
For example, we can create a new script file,
 /etc/profile.d/gf_shortcuts.sh
and add the following line to append /some/new/path to the global PATH:
   export PATH=$PATH:/Donnees/GIT/git_git-facile_gf_/gf_git_facile/gf_instructions    (or wherever the gf_instructions directory is

All of the scripts in /etc/profile.d/ will be executed when a new shell initializes. Therefore, we need to open a new shell for our global changes to take effect.

ajouter :
# ---------------------------------------------------------------------------
# add directory to the PATH to allow functions of  gf_ git_facile
PATH=$PATH:/Donnees/GIT/git_git-facile_gf_/gf_git_facile/gf_instructions
export PATH
# ---------------------------------------------------------------------------


   or add 
# ---------------------------------------------------------------------------
export PATH="$PATH:/Donnees/GIT/git_git-facile_gf_/gf_git_facile/gf_instructions:$PATH"
# ---------------------------------------------------------------------------
  at the end of 
 ~/.bashrc



*Other method not as good :*
We can also add our new path directly to the existing PATH in the /etc/environment file:
   PATH=<existing_PATH>:/some/new/path
The /etc/environment file isn’t a script file, it only contains simple variable assignments, and it’s less flexible than a script. Because of this, making PATH changes in /etc/environment is discouraged. We recommend adding a new script to /etc/profile.d instead.









		that should do it : in terminal type gf and then TAB ... TAB... TAB... should give you acces to all functions


## gf  user manual :

open a terminal at the root of your git local repo directory   ( ATTENTION, this is IMPORTANT )
and type the gf_ functions....    
if you are not in the root of local repo, some actions may only have impact on the fils in sub-directories (like standard GIT functions)
	(for instance, synthesis tables will show the names of all files modified, but gf actions would not modify their status...sad..)


start as an example by typing the instruction ***gf_working_info_status***
	or more simply type ***gf***   then ***TAB***  then ***w***   then ***TAB***   then ***i***   then ***TAB***   and then ***ENTER***


a whole list of gf functions is accessed by ***gf***   then ***TAB*** ***TAB*** ***TAB***    (very basic but usefull to newcomers in terminal use)


all gf functions should be self explicative. if required a little comment is added when calling the funtion...
...try it and hopefully this is enough..


**Details of the *TAB* use and organisation of the functions :**
to easily acces all gf functions, start typing in a terminal  
*gf*   
   and then *TAB*   
                 then the first letter of next word if you know it       
                 or *TAB TAB* untill all options are shown.

		then first letter of next word, and *TAB*
		or *TAB* *TAB*, ... and so forth... until you have your full name function.

function names are chosen so that each one begins with a different letter, making easy the above way of use.


- a function name usually start by the data status (working, index, head, fetch-head, upstream) printed in BLUE in the gf_visual_files-synthesis-table
 			followed by the flow action name shown by arrows (to, collect, ..) printed in GREEN for normal flow actions,
				or (back, discard, ...) printed in orange for reverse/cancel actions
			and eventually some arguments if required by the function.
- some general functions start by the category with which the is dealing :  branch, config, project, time, visual

- visual category provides the synthesis tables, and links to other usefull tools.

- gf_xx.. and gf_yy... may help to find the instruction one may be looking for.


  **note regarding special characters in the gf functions names :**
             _ (underscore character) separates segments of wordings of functions
             - (minus)  separates words within the same segment of wording              



            if it sounds complicated, just start using the TAB and it should all appear clearly...



## Details of instructions : (still under some changes at the moment.. try and you'll see for good)

gf_	  
gf_a-get-ready-to-use-gf	  
gf_branch_abort-merge	  
gf_branch_create_new	  
gf_branch_create_track	  
gf_branch_delete	  
gf_branch_info	  
gf_branch_merge	  
gf_branch_switch-head-CHECKOUT	  
gf_branch_unique-SQUASH-MERGE	  
gf_config_email	  
gf_config_gf-initialisation	  
gf_config_list	  
gf_config_mergetool	  
gf_config_name	  
gf_config_text-editor	  
gf_diff_head-local_against_fetch-head	  
gf_diff_head-local_against_upstream	  
gf_diff_working_against_fetch-head	  
gf_diff_working_against_head-local	  
gf_diff_working_against_index-staged	  
gf_diff_working_against_upstream	  
gf_external-SUBTREE_add_1-remote-creation	  
gf_external-SUBTREE_list-all-subtree	  
gf_fetch-head-copy_collect-in_head-local-MERGE	  
gf_fetch-head-copy_discard-cancel-fetch-from_upstream	  
gf_head-local_back-to_index-staged-RESET-SOFT-HEAD	  
gf_head-local_comment-tag_create-new	  
gf_head-local_comment-tag_list	  
gf_head-local_comment-tag_push-upstream	  
gf_head-local_discard-cancel-pull-from_fetch-head	  
gf_head-local_info_blame-last-change-line	  
gf_head-local_info_file-find-LS	  
gf_head-local_info_log	  
gf_head-local_info_reflog	  
gf_head-local_modify_commit-comment-AMEND	  
gf_head-local_rewind-time-back_commit-id	  
gf_head-local_rewind-time-back_date	  
gf_head-local_to_upstream-PUSH	  
gf_index-staged_back-to_working-RESET-not-for-pushed	  
gf_index-staged_info_status	  
gf_index-staged_to_head-local-COMMIT	  
gf_manual-MAN	  
gf_project_clone	  
gf_project_init-create-new-git-repo	  
gf_project_remote_add	  
gf_project_remote_set-url	  
gf_project_remote_view	  
gf_project_sub_module_add	  
gf_project_sub_module_init	  
gf_project_sub_module_status	  
gf_project_sub_module_update_all-RECURSIVE	  
gf_project_sub_module_update_one	  
gf_project_sub_tree_add_1-remote-creation	  
gf_project_sub_tree_add_2-SUBTREE-ADD	  
gf_project_sub_tree_collect-PULL	  
gf_project_sub_tree_diff-against-upstream	  
gf_project_sub_tree_list-all-subtree	  
gf_project_sub_tree_split-history	  
gf_project_sub_tree_to-upstream-PUSH	  
gf_rebase_abort	  
gf_rebase_continue	  
gf_rebase_skip	  
gf_stash_apply-to-working	  
gf_stash_branch-create-new	  
gf_stash_clean-away	  
gf_stash_list	  
gf_stash_pop-apply-to-working-and-delete	  
gf_stash_reverse-apply--cancel	  
gf_stash_show-detail	  
gf_time_rewind-back-detached-head_commit-id	  
gf_time_rewind-back-detached-head_date	  
gf_time_search_file-LOG-L_function	  
gf_time_search_file-LOG-L_lines	  
gf_upstream_back-to_previous-commits-REVERT	  
gf_upstream_collect-in_fetch-head-copy-FETCH	  
gf_upstream_fast-collect-in_head-local-PULL_rebase	  
gf_visual_	  
gf_visual_all-synthesis-table	  
gf_visual_all-synthesis-table-----namecheck	  
gf_visual_branches-synthesis-table	  
gf_visual_detail-show_branches	  
gf_visual_detail-show_diff	  
gf_visual_detail-show_files	  
gf_visual_detail-show_log-info-LOG	  
gf_visual_files-synthesis-table	  
gf_visual_graphic_branch-in-time	  
gf_visual_history_clear-delete	  
gf_visual_history_show	  
gf_visual_submodules-synthesis-table	  
gf_visual_tool_gui-GIT-GUI	  
gf_visual_tool_k-GITK	  
gf_visual_tool_mergetool	  
gf_visual_upstream-sources-synthesis-table-----namecheck	  
gf_working_clean-state-STASH	  
gf_working_fast-to_head-local-ADD-COMMIT	  
gf_working_info_status	  
gf_working_to_index-staged-ADD	  
gf_working_ultra-fast-to_upstream-ADD-COMMIT-PUSH	  
gf_xx-name-function-find	  
gf_yy-grep-word-search-within-files-GREP	  
  
and not directly usable functions :  
  
gf_---z-exemple-function	  
gf_-zfunc-full-prompt-branch-currentdir	  
gf_-zfunc-init-gf	  
gf_-zfunc-printdo	  
gf_-zfunc_resize-terminal	  
gf_-zfunc_shell_version	  
gf_--zfunc_shortmanual-print	  
gf_-zfunc-show_branches	  
gf_-zfunc-show_files	  
gf_-zfunc-show_global	  
gf_-zfunc-show_submodules	  
gf_-zfunc-show_submodules-A_FAIRE	  
gf_-zfunc-show_sub_trees	  
gf_-zvisual-all-storage-files	  
gf_--zzfunc-list-fileoflines-belong-subdir	  
gf_--zzfunc-status-file-belong-subdir	  
gf_--zzfunc-table-printfunc	  


  other gf_ functions added regularly... to be completed

-----------------------------------------------------------------------------------------------------------------

## appendix :

### licence details :   GNU GPL 3.0 or later




GNU GENERAL PUBLIC LICENSE

Version 3, 29 June 2007

Copyright © 2007 Free Software Foundation, Inc. <https://fsf.org/>

Everyone is permitted to copy and distribute verbatim copies of this license document, but changing it is not allowed.
Preamble

The GNU General Public License is a free, copyleft license for software and other kinds of works.

The licenses for most software and other practical works are designed to take away your freedom to share and change the works. By contrast, the GNU General Public License is intended to guarantee your freedom to share and change all versions of a program--to make sure it remains free software for all its users. We, the Free Software Foundation, use the GNU General Public License for most of our software; it applies also to any other work released this way by its authors. You can apply it to your programs, too.

When we speak of free software, we are referring to freedom, not price. Our General Public Licenses are designed to make sure that you have the freedom to distribute copies of free software (and charge for them if you wish), that you receive source code or can get it if you want it, that you can change the software or use pieces of it in new free programs, and that you know you can do these things.

To protect your rights, we need to prevent others from denying you these rights or asking you to surrender the rights. Therefore, you have certain responsibilities if you distribute copies of the software, or if you modify it: responsibilities to respect the freedom of others.

For example, if you distribute copies of such a program, whether gratis or for a fee, you must pass on to the recipients the same freedoms that you received. You must make sure that they, too, receive or can get the source code. And you must show them these terms so they know their rights.

Developers that use the GNU GPL protect your rights with two steps: (1) assert copyright on the software, and (2) offer you this License giving you legal permission to copy, distribute and/or modify it.

For the developers' and authors' protection, the GPL clearly explains that there is no warranty for this free software. For both users' and authors' sake, the GPL requires that modified versions be marked as changed, so that their problems will not be attributed erroneously to authors of previous versions.

Some devices are designed to deny users access to install or run modified versions of the software inside them, although the manufacturer can do so. This is fundamentally incompatible with the aim of protecting users' freedom to change the software. The systematic pattern of such abuse occurs in the area of products for individuals to use, which is precisely where it is most unacceptable. Therefore, we have designed this version of the GPL to prohibit the practice for those products. If such problems arise substantially in other domains, we stand ready to extend this provision to those domains in future versions of the GPL, as needed to protect the freedom of users.

Finally, every program is threatened constantly by software patents. States should not allow patents to restrict development and use of software on general-purpose computers, but in those that do, we wish to avoid the special danger that patents applied to a free program could make it effectively proprietary. To prevent this, the GPL assures that patents cannot be used to render the program non-free.

The precise terms and conditions for copying, distribution and modification follow.
TERMS AND CONDITIONS
0. Definitions.

“This License” refers to version 3 of the GNU General Public License.

“Copyright” also means copyright-like laws that apply to other kinds of works, such as semiconductor masks.

“The Program” refers to any copyrightable work licensed under this License. Each licensee is addressed as “you”. “Licensees” and “recipients” may be individuals or organizations.

To “modify” a work means to copy from or adapt all or part of the work in a fashion requiring copyright permission, other than the making of an exact copy. The resulting work is called a “modified version” of the earlier work or a work “based on” the earlier work.

A “covered work” means either the unmodified Program or a work based on the Program.

To “propagate” a work means to do anything with it that, without permission, would make you directly or secondarily liable for infringement under applicable copyright law, except executing it on a computer or modifying a private copy. Propagation includes copying, distribution (with or without modification), making available to the public, and in some countries other activities as well.

To “convey” a work means any kind of propagation that enables other parties to make or receive copies. Mere interaction with a user through a computer network, with no transfer of a copy, is not conveying.

An interactive user interface displays “Appropriate Legal Notices” to the extent that it includes a convenient and prominently visible feature that (1) displays an appropriate copyright notice, and (2) tells the user that there is no warranty for the work (except to the extent that warranties are provided), that licensees may convey the work under this License, and how to view a copy of this License. If the interface presents a list of user commands or options, such as a menu, a prominent item in the list meets this criterion.
1. Source Code.

The “source code” for a work means the preferred form of the work for making modifications to it. “Object code” means any non-source form of a work.

A “Standard Interface” means an interface that either is an official standard defined by a recognized standards body, or, in the case of interfaces specified for a particular programming language, one that is widely used among developers working in that language.

The “System Libraries” of an executable work include anything, other than the work as a whole, that (a) is included in the normal form of packaging a Major Component, but which is not part of that Major Component, and (b) serves only to enable use of the work with that Major Component, or to implement a Standard Interface for which an implementation is available to the public in source code form. A “Major Component”, in this context, means a major essential component (kernel, window system, and so on) of the specific operating system (if any) on which the executable work runs, or a compiler used to produce the work, or an object code interpreter used to run it.

The “Corresponding Source” for a work in object code form means all the source code needed to generate, install, and (for an executable work) run the object code and to modify the work, including scripts to control those activities. However, it does not include the work's System Libraries, or general-purpose tools or generally available free programs which are used unmodified in performing those activities but which are not part of the work. For example, Corresponding Source includes interface definition files associated with source files for the work, and the source code for shared libraries and dynamically linked subprograms that the work is specifically designed to require, such as by intimate data communication or control flow between those subprograms and other parts of the work.

The Corresponding Source need not include anything that users can regenerate automatically from other parts of the Corresponding Source.

The Corresponding Source for a work in source code form is that same work.
2. Basic Permissions.

All rights granted under this License are granted for the term of copyright on the Program, and are irrevocable provided the stated conditions are met. This License explicitly affirms your unlimited permission to run the unmodified Program. The output from running a covered work is covered by this License only if the output, given its content, constitutes a covered work. This License acknowledges your rights of fair use or other equivalent, as provided by copyright law.

You may make, run and propagate covered works that you do not convey, without conditions so long as your license otherwise remains in force. You may convey covered works to others for the sole purpose of having them make modifications exclusively for you, or provide you with facilities for running those works, provided that you comply with the terms of this License in conveying all material for which you do not control copyright. Those thus making or running the covered works for you must do so exclusively on your behalf, under your direction and control, on terms that prohibit them from making any copies of your copyrighted material outside their relationship with you.

Conveying under any other circumstances is permitted solely under the conditions stated below. Sublicensing is not allowed; section 10 makes it unnecessary.
3. Protecting Users' Legal Rights From Anti-Circumvention Law.

No covered work shall be deemed part of an effective technological measure under any applicable law fulfilling obligations under article 11 of the WIPO copyright treaty adopted on 20 December 1996, or similar laws prohibiting or restricting circumvention of such measures.

When you convey a covered work, you waive any legal power to forbid circumvention of technological measures to the extent such circumvention is effected by exercising rights under this License with respect to the covered work, and you disclaim any intention to limit operation or modification of the work as a means of enforcing, against the work's users, your or third parties' legal rights to forbid circumvention of technological measures.
4. Conveying Verbatim Copies.

You may convey verbatim copies of the Program's source code as you receive it, in any medium, provided that you conspicuously and appropriately publish on each copy an appropriate copyright notice; keep intact all notices stating that this License and any non-permissive terms added in accord with section 7 apply to the code; keep intact all notices of the absence of any warranty; and give all recipients a copy of this License along with the Program.

You may charge any price or no price for each copy that you convey, and you may offer support or warranty protection for a fee.
5. Conveying Modified Source Versions.

You may convey a work based on the Program, or the modifications to produce it from the Program, in the form of source code under the terms of section 4, provided that you also meet all of these conditions:

    a) The work must carry prominent notices stating that you modified it, and giving a relevant date.
    b) The work must carry prominent notices stating that it is released under this License and any conditions added under section 7. This requirement modifies the requirement in section 4 to “keep intact all notices”.
    c) You must license the entire work, as a whole, under this License to anyone who comes into possession of a copy. This License will therefore apply, along with any applicable section 7 additional terms, to the whole of the work, and all its parts, regardless of how they are packaged. This License gives no permission to license the work in any other way, but it does not invalidate such permission if you have separately received it.
    d) If the work has interactive user interfaces, each must display Appropriate Legal Notices; however, if the Program has interactive interfaces that do not display Appropriate Legal Notices, your work need not make them do so.

A compilation of a covered work with other separate and independent works, which are not by their nature extensions of the covered work, and which are not combined with it such as to form a larger program, in or on a volume of a storage or distribution medium, is called an “aggregate” if the compilation and its resulting copyright are not used to limit the access or legal rights of the compilation's users beyond what the individual works permit. Inclusion of a covered work in an aggregate does not cause this License to apply to the other parts of the aggregate.
6. Conveying Non-Source Forms.

You may convey a covered work in object code form under the terms of sections 4 and 5, provided that you also convey the machine-readable Corresponding Source under the terms of this License, in one of these ways:

    a) Convey the object code in, or embodied in, a physical product (including a physical distribution medium), accompanied by the Corresponding Source fixed on a durable physical medium customarily used for software interchange.
    b) Convey the object code in, or embodied in, a physical product (including a physical distribution medium), accompanied by a written offer, valid for at least three years and valid for as long as you offer spare parts or customer support for that product model, to give anyone who possesses the object code either (1) a copy of the Corresponding Source for all the software in the product that is covered by this License, on a durable physical medium customarily used for software interchange, for a price no more than your reasonable cost of physically performing this conveying of source, or (2) access to copy the Corresponding Source from a network server at no charge.
    c) Convey individual copies of the object code with a copy of the written offer to provide the Corresponding Source. This alternative is allowed only occasionally and noncommercially, and only if you received the object code with such an offer, in accord with subsection 6b.
    d) Convey the object code by offering access from a designated place (gratis or for a charge), and offer equivalent access to the Corresponding Source in the same way through the same place at no further charge. You need not require recipients to copy the Corresponding Source along with the object code. If the place to copy the object code is a network server, the Corresponding Source may be on a different server (operated by you or a third party) that supports equivalent copying facilities, provided you maintain clear directions next to the object code saying where to find the Corresponding Source. Regardless of what server hosts the Corresponding Source, you remain obligated to ensure that it is available for as long as needed to satisfy these requirements.
    e) Convey the object code using peer-to-peer transmission, provided you inform other peers where the object code and Corresponding Source of the work are being offered to the general public at no charge under subsection 6d.

A separable portion of the object code, whose source code is excluded from the Corresponding Source as a System Library, need not be included in conveying the object code work.

A “User Product” is either (1) a “consumer product”, which means any tangible personal property which is normally used for personal, family, or household purposes, or (2) anything designed or sold for incorporation into a dwelling. In determining whether a product is a consumer product, doubtful cases shall be resolved in favor of coverage. For a particular product received by a particular user, “normally used” refers to a typical or common use of that class of product, regardless of the status of the particular user or of the way in which the particular user actually uses, or expects or is expected to use, the product. A product is a consumer product regardless of whether the product has substantial commercial, industrial or non-consumer uses, unless such uses represent the only significant mode of use of the product.

“Installation Information” for a User Product means any methods, procedures, authorization keys, or other information required to install and execute modified versions of a covered work in that User Product from a modified version of its Corresponding Source. The information must suffice to ensure that the continued functioning of the modified object code is in no case prevented or interfered with solely because modification has been made.

If you convey an object code work under this section in, or with, or specifically for use in, a User Product, and the conveying occurs as part of a transaction in which the right of possession and use of the User Product is transferred to the recipient in perpetuity or for a fixed term (regardless of how the transaction is characterized), the Corresponding Source conveyed under this section must be accompanied by the Installation Information. But this requirement does not apply if neither you nor any third party retains the ability to install modified object code on the User Product (for example, the work has been installed in ROM).

The requirement to provide Installation Information does not include a requirement to continue to provide support service, warranty, or updates for a work that has been modified or installed by the recipient, or for the User Product in which it has been modified or installed. Access to a network may be denied when the modification itself materially and adversely affects the operation of the network or violates the rules and protocols for communication across the network.

Corresponding Source conveyed, and Installation Information provided, in accord with this section must be in a format that is publicly documented (and with an implementation available to the public in source code form), and must require no special password or key for unpacking, reading or copying.
7. Additional Terms.

“Additional permissions” are terms that supplement the terms of this License by making exceptions from one or more of its conditions. Additional permissions that are applicable to the entire Program shall be treated as though they were included in this License, to the extent that they are valid under applicable law. If additional permissions apply only to part of the Program, that part may be used separately under those permissions, but the entire Program remains governed by this License without regard to the additional permissions.

When you convey a copy of a covered work, you may at your option remove any additional permissions from that copy, or from any part of it. (Additional permissions may be written to require their own removal in certain cases when you modify the work.) You may place additional permissions on material, added by you to a covered work, for which you have or can give appropriate copyright permission.

Notwithstanding any other provision of this License, for material you add to a covered work, you may (if authorized by the copyright holders of that material) supplement the terms of this License with terms:

    a) Disclaiming warranty or limiting liability differently from the terms of sections 15 and 16 of this License; or
    b) Requiring preservation of specified reasonable legal notices or author attributions in that material or in the Appropriate Legal Notices displayed by works containing it; or
    c) Prohibiting misrepresentation of the origin of that material, or requiring that modified versions of such material be marked in reasonable ways as different from the original version; or
    d) Limiting the use for publicity purposes of names of licensors or authors of the material; or
    e) Declining to grant rights under trademark law for use of some trade names, trademarks, or service marks; or
    f) Requiring indemnification of licensors and authors of that material by anyone who conveys the material (or modified versions of it) with contractual assumptions of liability to the recipient, for any liability that these contractual assumptions directly impose on those licensors and authors.

All other non-permissive additional terms are considered “further restrictions” within the meaning of section 10. If the Program as you received it, or any part of it, contains a notice stating that it is governed by this License along with a term that is a further restriction, you may remove that term. If a license document contains a further restriction but permits relicensing or conveying under this License, you may add to a covered work material governed by the terms of that license document, provided that the further restriction does not survive such relicensing or conveying.

If you add terms to a covered work in accord with this section, you must place, in the relevant source files, a statement of the additional terms that apply to those files, or a notice indicating where to find the applicable terms.

Additional terms, permissive or non-permissive, may be stated in the form of a separately written license, or stated as exceptions; the above requirements apply either way.
8. Termination.

You may not propagate or modify a covered work except as expressly provided under this License. Any attempt otherwise to propagate or modify it is void, and will automatically terminate your rights under this License (including any patent licenses granted under the third paragraph of section 11).

However, if you cease all violation of this License, then your license from a particular copyright holder is reinstated (a) provisionally, unless and until the copyright holder explicitly and finally terminates your license, and (b) permanently, if the copyright holder fails to notify you of the violation by some reasonable means prior to 60 days after the cessation.

Moreover, your license from a particular copyright holder is reinstated permanently if the copyright holder notifies you of the violation by some reasonable means, this is the first time you have received notice of violation of this License (for any work) from that copyright holder, and you cure the violation prior to 30 days after your receipt of the notice.

Termination of your rights under this section does not terminate the licenses of parties who have received copies or rights from you under this License. If your rights have been terminated and not permanently reinstated, you do not qualify to receive new licenses for the same material under section 10.
9. Acceptance Not Required for Having Copies.

You are not required to accept this License in order to receive or run a copy of the Program. Ancillary propagation of a covered work occurring solely as a consequence of using peer-to-peer transmission to receive a copy likewise does not require acceptance. However, nothing other than this License grants you permission to propagate or modify any covered work. These actions infringe copyright if you do not accept this License. Therefore, by modifying or propagating a covered work, you indicate your acceptance of this License to do so.
10. Automatic Licensing of Downstream Recipients.

Each time you convey a covered work, the recipient automatically receives a license from the original licensors, to run, modify and propagate that work, subject to this License. You are not responsible for enforcing compliance by third parties with this License.

An “entity transaction” is a transaction transferring control of an organization, or substantially all assets of one, or subdividing an organization, or merging organizations. If propagation of a covered work results from an entity transaction, each party to that transaction who receives a copy of the work also receives whatever licenses to the work the party's predecessor in interest had or could give under the previous paragraph, plus a right to possession of the Corresponding Source of the work from the predecessor in interest, if the predecessor has it or can get it with reasonable efforts.

You may not impose any further restrictions on the exercise of the rights granted or affirmed under this License. For example, you may not impose a license fee, royalty, or other charge for exercise of rights granted under this License, and you may not initiate litigation (including a cross-claim or counterclaim in a lawsuit) alleging that any patent claim is infringed by making, using, selling, offering for sale, or importing the Program or any portion of it.
11. Patents.

A “contributor” is a copyright holder who authorizes use under this License of the Program or a work on which the Program is based. The work thus licensed is called the contributor's “contributor version”.

A contributor's “essential patent claims” are all patent claims owned or controlled by the contributor, whether already acquired or hereafter acquired, that would be infringed by some manner, permitted by this License, of making, using, or selling its contributor version, but do not include claims that would be infringed only as a consequence of further modification of the contributor version. For purposes of this definition, “control” includes the right to grant patent sublicenses in a manner consistent with the requirements of this License.

Each contributor grants you a non-exclusive, worldwide, royalty-free patent license under the contributor's essential patent claims, to make, use, sell, offer for sale, import and otherwise run, modify and propagate the contents of its contributor version.

In the following three paragraphs, a “patent license” is any express agreement or commitment, however denominated, not to enforce a patent (such as an express permission to practice a patent or covenant not to sue for patent infringement). To “grant” such a patent license to a party means to make such an agreement or commitment not to enforce a patent against the party.

If you convey a covered work, knowingly relying on a patent license, and the Corresponding Source of the work is not available for anyone to copy, free of charge and under the terms of this License, through a publicly available network server or other readily accessible means, then you must either (1) cause the Corresponding Source to be so available, or (2) arrange to deprive yourself of the benefit of the patent license for this particular work, or (3) arrange, in a manner consistent with the requirements of this License, to extend the patent license to downstream recipients. “Knowingly relying” means you have actual knowledge that, but for the patent license, your conveying the covered work in a country, or your recipient's use of the covered work in a country, would infringe one or more identifiable patents in that country that you have reason to believe are valid.

If, pursuant to or in connection with a single transaction or arrangement, you convey, or propagate by procuring conveyance of, a covered work, and grant a patent license to some of the parties receiving the covered work authorizing them to use, propagate, modify or convey a specific copy of the covered work, then the patent license you grant is automatically extended to all recipients of the covered work and works based on it.

A patent license is “discriminatory” if it does not include within the scope of its coverage, prohibits the exercise of, or is conditioned on the non-exercise of one or more of the rights that are specifically granted under this License. You may not convey a covered work if you are a party to an arrangement with a third party that is in the business of distributing software, under which you make payment to the third party based on the extent of your activity of conveying the work, and under which the third party grants, to any of the parties who would receive the covered work from you, a discriminatory patent license (a) in connection with copies of the covered work conveyed by you (or copies made from those copies), or (b) primarily for and in connection with specific products or compilations that contain the covered work, unless you entered into that arrangement, or that patent license was granted, prior to 28 March 2007.

Nothing in this License shall be construed as excluding or limiting any implied license or other defenses to infringement that may otherwise be available to you under applicable patent law.
12. No Surrender of Others' Freedom.

If conditions are imposed on you (whether by court order, agreement or otherwise) that contradict the conditions of this License, they do not excuse you from the conditions of this License. If you cannot convey a covered work so as to satisfy simultaneously your obligations under this License and any other pertinent obligations, then as a consequence you may not convey it at all. For example, if you agree to terms that obligate you to collect a royalty for further conveying from those to whom you convey the Program, the only way you could satisfy both those terms and this License would be to refrain entirely from conveying the Program.
13. Use with the GNU Affero General Public License.

Notwithstanding any other provision of this License, you have permission to link or combine any covered work with a work licensed under version 3 of the GNU Affero General Public License into a single combined work, and to convey the resulting work. The terms of this License will continue to apply to the part which is the covered work, but the special requirements of the GNU Affero General Public License, section 13, concerning interaction through a network will apply to the combination as such.
14. Revised Versions of this License.

The Free Software Foundation may publish revised and/or new versions of the GNU General Public License from time to time. Such new versions will be similar in spirit to the present version, but may differ in detail to address new problems or concerns.

Each version is given a distinguishing version number. If the Program specifies that a certain numbered version of the GNU General Public License “or any later version” applies to it, you have the option of following the terms and conditions either of that numbered version or of any later version published by the Free Software Foundation. If the Program does not specify a version number of the GNU General Public License, you may choose any version ever published by the Free Software Foundation.

If the Program specifies that a proxy can decide which future versions of the GNU General Public License can be used, that proxy's public statement of acceptance of a version permanently authorizes you to choose that version for the Program.

Later license versions may give you additional or different permissions. However, no additional obligations are imposed on any author or copyright holder as a result of your choosing to follow a later version.
15. Disclaimer of Warranty.

THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM “AS IS” WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
16. Limitation of Liability.

IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
17. Interpretation of Sections 15 and 16.

If the disclaimer of warranty and limitation of liability provided above cannot be given local legal effect according to their terms, reviewing courts shall apply local law that most closely approximates an absolute waiver of all civil liability in connection with the Program, unless a warranty or assumption of liability accompanies a copy of the Program in return for a fee.

END OF TERMS AND CONDITIONS
How to Apply These Terms to Your New Programs

If you develop a new program, and you want it to be of the greatest possible use to the public, the best way to achieve this is to make it free software which everyone can redistribute and change under these terms.

To do so, attach the following notices to the program. It is safest to attach them to the start of each source file to most effectively state the exclusion of warranty; and each file should have at least the “copyright” line and a pointer to where the full notice is found.

    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

Also add information on how to contact you by electronic and paper mail.

If the program does terminal interaction, make it output a short notice like this when it starts in an interactive mode:

    <program>  Copyright (C) <year>  <name of author>
    This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
    This is free software, and you are welcome to redistribute it
    under certain conditions; type `show c' for details.

The hypothetical commands `show w' and `show c' should show the appropriate parts of the General Public License. Of course, your program's commands might be different; for a GUI interface, you would use an “about box”.

You should also get your employer (if you work as a programmer) or school, if any, to sign a “copyright disclaimer” for the program, if necessary. For more information on this, and how to apply and follow the GNU GPL, see <https://www.gnu.org/licenses/>.

The GNU General Public License does not permit incorporating your program into proprietary programs. If your program is a subroutine library, you may consider it more useful to permit linking proprietary applications with the library. If this is what you want to do, use the GNU Lesser General Public License instead of this License. But first, please read <https://www.gnu.org/licenses/why-not-lgpl.html>.
